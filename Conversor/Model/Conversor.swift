//
//  Conversor.swift
//  Conversor
//
//  Created by Mario Alejandro on 2/19/19.
//  Copyright © 2019 Casa. All rights reserved.
//

import Foundation
//Acá el modelo que solucionará el problema de conversión
class Conversor: NSObject{

    /// Esta función se encargará de las conversiones de unidades
    ///
    /// - Parameters:
    ///   - from: desde que unidad
    ///   - amount: cantidad a convertir
    ///   - to: a cual unidad convertir
    /// - Returns: retornará el valor de la conversión
    func convert(from:Int, amount: Int, to: Int) -> Double {
        //diccionario temporal
        var unitiesDictionary: NSDictionary?
        
        //Abrimos el plist con las unidades y llenamos el diccionario
        if let path = Bundle.main.path(forResource: "Unidades", ofType: "plist") {
            unitiesDictionary = NSDictionary(contentsOfFile: path)
        }
        
        //Convertimos desde el String al formato de clave o Key del diccionario
        let unityKey = "\(from)to\(to)"
        
        //obtenemos el factor para multiplicar, primero siempre castear (as!) con el mismo tipo del plist creado, luego convertir
        let factor = (unitiesDictionary?.value(forKey: unityKey) as! NSNumber).doubleValue
      
        return Double(amount) * factor
    }
    
    
}
