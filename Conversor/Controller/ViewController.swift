//
//  ViewController.swift
//  Conversor
//
//  Created by Mario Alejandro on 2/19/19.
//  Copyright © 2019 Casa. All rights reserved.
//

import UIKit

class ViewController: UIViewController,  UIPickerViewDelegate, UIPickerViewDataSource {
   
    var fromUnity = 0
    var toUnity = 0
    
    var arrayUnities = ["metros", "kilómetros","yardas", "centímetros","pies", "pulgadas"]
    
    @IBOutlet weak var amountTextfield: UITextField!
    @IBOutlet weak var convertButton: UIButton!
    var distanceValue : Int = 0
    @IBOutlet weak var pickerFrom: UIPickerView!
    @IBOutlet weak var pickerTo: UIPickerView!
    @IBOutlet weak var finalValueConvertedLabel: UILabel!
    
    //Esto es nuevo, mediante una jerarquía donde View ésta View es el padre (ver storboard) dejamos de hijos algunas gráficas y títulos y así escondemos y mostramos todo el resultado de una vez
    @IBOutlet weak var resultsView: UIView!
    //Mostramos la unidad a convertir en un label dentro del resultado
    @IBOutlet weak var selectedUnityLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()

        //Estos delegates son obligatotios en pickers y tableViews, permiten asociar los métodos de llenado de datos y de eventos a los que correspondan
        pickerFrom.delegate = self
        pickerTo.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func convertionButtonPressed(_ sender: Any) {
        
        // si el valor del textfield es vacio o inválido dará -1
        distanceValue = Int(amountTextfield.text!) ?? -1
        print( "Texto: \(distanceValue) ")
        
        //validamos si ha ingresado numeros
        if (distanceValue == -1){
            //No ha ingresado nada, mostramos mensaje flotante con alertController y retornamos con return saliendo del código sin hacer nada más :/
            
            let alertView = UIAlertController(title: "Ingrese datos", message: "Debe ingresar la cantidad", preferredStyle: .alert)
            
            alertView.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
       
            self.present(alertView, animated: true)

            //arrancamos de éste código con return
            return
        }
        
        //Esto se ejecuta sólo si el valor es válido
        
         fromUnity = self.pickerFrom.selectedRow(inComponent: 0)
        
        toUnity = self.pickerTo.selectedRow(inComponent: 0)
        
        
        if (fromUnity == toUnity){
            
            let alertView = UIAlertController(title: "Misma unidad", message: "La unidad de conversión debe ser diferente", preferredStyle: .alert)
            
            alertView.addAction(UIAlertAction(title: "Entendido", style: .default, handler: nil))
            
            self.present(alertView, animated: true)
            
            //arrancamos de éste código con return
            return
        }
        print("Convertir de: \(fromUnity) a: \(toUnity)  ")
        
        //Creamos el modelo
        let conversor = Conversor()
        
        //Y guardamos el resultado
        let result = conversor.convert(from: fromUnity, amount: distanceValue, to: toUnity)
        
        print("Result: \(result)  ")
        
        //Mostramos el resultado en el label
        finalValueConvertedLabel.text = String(result)
        
        //Esto pondrá el texto de la unidad a convertir que corresponda
       selectedUnityLabel.text = arrayUnities[toUnity]
        
        //Mostramos la vista de resultado del label escondida inicialmente
      self.resultsView.isHidden = false
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // Cuantas filas tiene el pickerView
        return self.arrayUnities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        // Llenamos los títulos de las filas con el array
        return self.arrayUnities[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        // Cuantas columnas tiene el pickerView
        return 1
    }
  
}

//Ésta es una extensión, se escribe afuera del código y es integra a todos los ViewControllers de una! Permite extender una funcionalidad, en éste caso esconderemos el teclado si hacemos tap en cualquier parte de la pantalla. La llamaremos en ViewDidLoad()
extension UIViewController
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
